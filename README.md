# Mark as Exclude Context Menu

## Features

* Menu: **Mark as excluded**: Add the selected file or folder to the exlude list in the ~/.vscode/settings.json file

## Using

* Right click on a folder or file in the Explorer, choose **Mark as excluded**

![Preview](images/preview.png)

## License

[MIT](LICENSE.md)
