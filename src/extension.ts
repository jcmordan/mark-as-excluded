'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import * as fs from 'fs';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	const filesExcludeKey = 'files.exclude';
	const filesIgnoreKey = 'search.exclude';
	function getUserConfigurationFilePath(currentWorkspaceFolder: string) {
		return currentWorkspaceFolder + '/.vscode/settings.json';
	}

	function getWorkspaceConfiguration(currentWorkspaceFolder: string) {
		const configFile = getUserConfigurationFilePath(currentWorkspaceFolder);
		let jsonData = '{}';
		try {
			fs.accessSync(configFile);
			jsonData = fs.readFileSync(configFile, { encoding: 'utf8' });
		} catch (error) {
			// do nothing, the file will be created
		}
		return JSON.parse(jsonData);
	}

	function saveConfiguration(currentWorkspaceFolder: string, config: any) {
		const configFile = getUserConfigurationFilePath(currentWorkspaceFolder);
		let jsonData = JSON.stringify(config, null, 2);
		jsonData = jsonData.replace(/^[^{]+|[^}]+$/, '').replace(/(.+?[^:])\/\/.+$/gm, '$1');

		try {
			fs.writeFileSync(configFile, jsonData, { encoding: 'utf8' });
			return true;
		} catch (error) {
			vscode.window.showInformationMessage('The file could not be exclued, verify you have access');
			return false;
		}
	}

	function getCurrentWorkspaceFolder(filePath: string): string {
		let result;
		if (vscode.workspace.hasOwnProperty('workspaceFolders')) {
			result = vscode.workspace['workspaceFolders']
				.filter((wf) => filePath.indexOf(wf.uri.fsPath) > -1)
				.map((wf) => wf.uri.fsPath);
		}
		return result[0] || vscode.workspace.rootPath;
	}

	function validateSelectedElement(e) {
		if (!e) {
			vscode.window.showInformationMessage('Nothing is selected!');
			return false;
		} else {
			return true;
		}
	}

	function applyConfiguration(e: vscode.Uri, config: any, key: string) {
        let element = '**' + e.path.substring(vscode.workspace.rootPath.length + 1);
        if (!config[key]) {
            config[key] = {};
        }
        if (!config[key][element]) {
            config[key][element] = true;
		}
		
		const currentWorkspaceFolder = getCurrentWorkspaceFolder(e.fsPath)

        if (saveConfiguration(currentWorkspaceFolder, config)) {
            if (key === filesExcludeKey) {
                vscode.window.showInformationMessage('Excluded from workspace: ' + element);
            }
            else {
                vscode.window.showInformationMessage('Excluded from search: ' + element);
            }
        }
    }

	// The explorer/context menu contribution receives the URI to the file/folder
	const excludeCommand = vscode.commands.registerCommand('extension.exclude', (e: vscode.Uri) => {
		if (!validateSelectedElement(e)) {
			return;
		}
		const workspaceFolder = getCurrentWorkspaceFolder(e.fsPath)
		const config = getWorkspaceConfiguration(workspaceFolder);
		applyConfiguration(e, config, filesExcludeKey);
	});
	const ignoreCommand = vscode.commands.registerCommand('extension.ignore', (e: vscode.Uri) => {
		if (!validateSelectedElement(e)) {
			return;
		}
		const workspaceFolder = getCurrentWorkspaceFolder(e.fsPath)
		const config = getWorkspaceConfiguration(workspaceFolder);
		applyConfiguration(e, config, filesIgnoreKey);
	});
	context.subscriptions.push(excludeCommand);
	context.subscriptions.push(ignoreCommand);
}

// this method is called when your extension is deactivated
export function deactivate() {}
